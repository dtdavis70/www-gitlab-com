---
layout: handbook-page-toc
title: "Preferred Companies"
description: "We use this preferred companies list for two reasons; it's used internally by our sourcing team, plus we hope that if you work, or have worked, at one of the following companies you know we may reach out to you."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We use this preferred companies list for two reasons; it's used internally by our sourcing team, plus we hope that if you work, or have worked, at one of the following companies you know we may reach out to you. Of course, this list is not definitive and it is not necessary to have this experience to work at GitLab.

We try to avoid mentioning [partners](/partners/technology-partners/) on this page because it might hurt the relationship.

## DevOps Stages

GitLab is a single application for the complete DevOps Lifecycle. As such, we're highly interested in hiring individuals with experience from companies who create products related to specific parts of this lifecycle.

### Common

* AirBnB
* Alcide
* Amazon
* Capital One
* Chef
* Datera
* Dell
* DigitalOcean
* Docker
* f5
* Facebook
* Google
* GitHub
* Heroku
* Joyent
* Lyft
* Mesosphere
* Microsoft
* Netflix
* New Relic
* NPM
* Oracle
* Palo Alto Networks
* Pivotal
* Puppet
* Qualcomm
* Rackspace
* RedHat
* Shopify
* Sonarcube
* Stripe
* Sumologic
* Twilio
* Twitch
* Uber
* VMware
* Workday
* WP Engine

### Manage

#### Access

* Auth0
* Azure AD
* Broadcom
* Evidian
* ForgeRock
* IBM
* Idaptive
* Micro Focus
* Okta
* OneLogin
* Optimal IdM
* Oracle
* Ping Identity
* SailPoint
* SecureAuth
* Symantec

#### Compliance

* AuditBoard
* Cobalt
* Collabnet
* Galvanize
* LogicGate
* Micro Focus
* Reciprocitylabs
* ServiceNow
* ZenGRC

#### Analytics

* Code Climate
* Elastic
* Micro Focus
* Pluralsight
* Plutora
* Splunk
* Tasktop 
* XebiaLabs

#### Import

* AgileCraft
* Aha!
* Asana
* CollabNet
* GitHub
* Jenkins
* Rally
* Tasktop
* Trello
* VersionOne
* XebiaLabs

#### Spaces

* Asana
* GitHub
* JetBrains
* ServiceNow


### Plan

#### Project 

* Asana
* Basecamp
* Clarizen
* LiquidPlanner
* Mavenlink
* Monday.com
* Microfocus
* Planview
* Podio
* Quickbase
* Quip
* Sciforma
* Smartsheet
* Sopheon
* Targetprocess
* Trello
* Upland
* Workfront
* Wrike

#### Portfolio

* AHA.io
* Changeview
* Microfocus
* Planview
* Planisware
* Productboard
* Rally
* Roadmunk
* Sciforma
* Trello


#### Certify

* Jama Connect
* IBM Rational DOORS
* Intercom
* qTest
* Test Rail
* HPQC
* HubSpot
* Zendesk
* Freshdesk


### Create

#### Source Code

* Azure
* Gerrit
* GitHub
* Perforce
* Phacility

#### Knowledge

* Abstract
* Adobe
* Avocode
* DropBox
* Figma
* Framer
* GitHub
* Google Docs
* Invision Studio
* Notion
* Roam Research
* Storybook UI
* UX Pin
* Webflow
* Zeplin

#### Editor

* Blocks
* Cloud9
* Coder
* Codesandbox
* Gist.io
* GitHub
* Gitpod
* Koding
* Repl.it
* StackBlitz
* Theia
* Visual Studio One
* Contentful
* Forestry.io
* Gatsby
* Gridsome
* Jekyll+
* Kontent.ai
* Netlify CMS
* Nuxt
* Prose
* Siteleaf
* Sourcebit
* Squarespace
* Stackbit
* TinaCMS
* Toast UI
* Webflow

#### Gitaly

* GitHub
* Perforce

#### Gitter

* Circle
* Comradery
* Discord
* Freenode

### Ecosystem

#### Integrations

* GitHub
* HashiCorp
* IFTTT
* RedHat
* Salesforce
* Slack
* Stripe
* Tasktop
* Twilio
* Zapier

### Verify

#### Continuous Integration

* GitHub
* Jenkins
* Bamboo
* Travis CI
* CircleCI
* CodeFresh
* Codeship
* JetBrains/TeamCity
* Harness
* JFrog
* Buildkite
* BitBucket

#### Testing

* CodeClimate
* Codacy
* SonarQube
* Coverity
* DiffBlue
* Marker.io
* Eggplant

### Release

* Codeship
* Netlify
* Zeit
* Plutora
* CodeFresh
* Cloudbees
* LaunchDarkly
* JFrog
* Spinnaker
* Urbancode
* XebiaLabs
* DeployHQ
* Harness
* ArgoCD

### Package

* codefresh
* goHarbour
* JFrog
* NPM
* Quay
* Sonatype

### Configure

* Serverless
* Cloudreach

### Monitor

* Splunk/VictorOps/SignalFx
* PagerDuty
* ServiceNow 
* Datadog
* Grafana Labs
* Honeycomb
* Lightstep
* OpenTelemetry
* Prometheus
* Jaeger
* Dynatrace
* Elastic
* Influxdata
* Netsil
* New Relic
* AppDynamics/Cisco

### Secure

* Anchore
* Appscan
* Burp Suite 
* Blackduck
* Checkmarx
* Coverity
* Kata Containers
* Micro Focus
* Microsoft
* Snyk
* Sonarqube
* Synopsys
* Tenable 
* Twistlock
* Veracode
* Whitesource


### Protect

#### Container Security

* [Akamai](https://www.akamai.com/us/en/resources/application-firewall.jsp)
* [Alertlogic](https://www.alertlogic.com/solutions/web-application-firewall/)
* [Alibaba Cloud WAF](https://www.alibabacloud.com/product/waf)
* [Anchor Enterprise](https://anchore.com/enterprise/)
* [Aqua](https://www.aquasec.com/products/aqua-cloud-native-security-platform/)
* [Arxan](https://www.arxan.com/)
* [Attivo](https://attivonetworks.com/product/deception-technology/)
* [Axway](https://marketplace.axway.com/apps/187256/web-access-firewall---xss-functional-example#!overview)
* [Barracuda Networks](https://www.barracuda.com/products/webapplicationfirewall)
* [Capsulate Protect](https://capsule8.com/resource/capsule8-protect-product-overview/)
* [Check Point Container Security](https://www.checkpoint.com/solutions/container-security/)
* [Citrix Web App Firewall](/handbook/product/categories/#protect-stage)
* [Cloudflare](https://www.cloudflare.com/waf/)
* [CloudPassage Container Secure](https://pages.cloudpassage.com/rs/857-FXQ-213/images/Container-Secure-data-sheet_03082018-1.pdf?_ga=2.80309470.834074949.1589860053-306212359.1589860053)
* [Comodo](https://www.comodo.com/)
* [Corero](https://www.corero.com/)
* [CyberArk](https://www.cyberark.com/)
* [Deepfence](https://deepfence.io/kubernetes-security/)
* [Exabeam](https://www.exabeam.com/)
* [Fortinet](https://www.fortinet.com/products/web-application-firewall/fortiweb.html)
* [Microsoft Azure WAF](https://docs.microsoft.com/en-us/azure/application-gateway/waf-overview)
* [NeuVector](https://neuvector.com/)
* [Oracle WAF](https://www.oracle.com/cloud/security/cloud-services/web-application-firewall.html)
* [Polyverse](https://polyverse.com/products/polymorphing-linux/)
* [Radware](https://www.radware.com/products/appwall/)
* [Signal Sciences](https://www.signalsciences.com/)
* [Stack Rox](https://www.stackrox.com/)
* [Twistlock](https://www.twistlock.com/)


### Enablement

* Look under [common](#common)

### Growth

Companies with a strong reputation for running effective Growth teams and experiments.

* AirBnb
* Facebook
* HubSpot
* Netflix
* Slack
* Spotify
* Twitter
* Uber

## Company Experience Based on Functional Area

Some companies demonstrate excellence in specific function. So we value experience from there regardless of how similar their product is to ours.

### Engineering Division

#### Development Department

Companies with strong reputation in product development reputation and/or SaaS solutions.

* Look under [DevOps Stages](#devops-stages)
* Adobe
* Salesforce

#### Infrastructure Department

* Facebook
* Google
* Amazon
* Twitter
* Spotify

#### Quality Department

Companies with a strong reputation in test engineering, and engineering productivity.

* Walmart Labs
* BlackBoard
* Expedia
* Houzz
* Credit Karma
* GitHub
* CircleCI
* VictorOps
* SauceLabs
* Splunk
* Puppet
* Slack
* SalesForce
* Rackspace
* Workday
* Amazon
* Google
* Facebook
* Microsoft
* NetFlix
* VMware
* ThoughtWorks

#### Security Department

We strive to hire globally at GitLab, and the Security Department is very much focused on hiring globally. Our Security top-level objectives span an extremely broad expanse of domain knowledge. We've found that hiring from companies that have established security teams works best for bringing on the best people. The companies most likely to have established security teams are Global 2000 companies. 

* [Global 2000 Companies](https://www.forbes.com/global2000/)

#### Customer Support Department

* Rackspace
* GoodData
* CircleCI
* New Relic
* Zendesk
* Digital Ocean
* DreamHost
* NexGen Technologies
* Optiva
* WP Engine

#### UX Department

* AppDynamics
* Chartbeat
* Chartio
* DigitalOcean
* Fivetran
* HubSpot
* IBM
* Looker
* Palintir
* Sumo Logic
* Segment
* Sentry.io
* Stack Overflow
* Wavefront by VMware
* WeWork

#### Sales -  in addition to [Devops Competitors](/devops-tools/) 

* AppDynamics
* AWS
* Checkpoint Software
* Cloudera
* Cloudflare
* Docker
* Dynatrace
* Elastic
* Electric Cloud
* Hashicorp
* LaunchDarkly
* NetApp
* New Relic
* Nutanix
* Opentext
* Palo Alto Networks
* Pivotal Software
* Qlik
* Red Hat
* Servicenow
* Software AG
* Splunk
* SUSE
* Trustwave
* VMware
* Zenlabs

